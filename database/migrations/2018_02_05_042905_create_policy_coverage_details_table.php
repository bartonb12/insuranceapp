<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyCoverageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_coverage_details', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('insurancePolicyId')->nullable();
            $table->string('name')->nullable();
            $table->string('details')->nullable();
            $table->integer('preferredProviderAmount')->nullable();
            $table->integer('nonPreferredProviderAmount')->nullable();
            $table->string('preferredProviderNotes')->nullable();
            $table->string('nonPreferredProviderNotes')->nullable();
            $table->string('preferredProviderAmountText')->nullable();
            $table->string('nonPreferredProviderAmountText')->nullable();
            $table->string('linkToPdf')->nullable();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_coverage_details');
    }
}
