<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsurancePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_policies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('policyNumber')->nullable();
            $table->integer('insuranceProviderId')->nullable();
            $table->string('policyType')->nullable();
            $table->integer('costToEmployee')->nullable();
            $table->integer('costToEmployer')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_policies');
    }
}
