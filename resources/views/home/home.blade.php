@extends('home.partials.master')


@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

<header class="masthead">
  <div class="container h-100">
    <div class="row h-100">
      <div class="col-lg-12 my-auto">
        <div class="header-content mx-auto">
          <form class="" action="/benefit"  method="post">
            {{ csrf_field() }}

          <h1 class="mb-5">What can we help you with?</h1>
          <input class="typeahead form-control" type="text" name='search'>
          <br>
          <input type="submit" class="btn btn-outline btn-xl js-scroll-trigger" value='Find Care Providers!'></input>

          </form>
        </div>
      </div>
    </div>
  </div>
</header>

<section class="features" id="features">
  <div class="container">
    <div class="section-heading text-center">
      <h2>Browse Benefits by Category</h2>
      <p class="text-muted">Check out what you can do with Insurapal!</p>
      <hr>
    </div>
    <div class="row">
      <div class="col-lg-12 my-auto">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-6">
              <div class="feature-item">
                <a href="#"><i class="icon-screen-smartphone text-primary"></i></a>
                <h3>Dental</h3>
                <p class="text-muted"></p>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="feature-item">
                <i class="icon-location-pin text-primary"></i>
                <h3>Health</h3>
                <p class="text-muted"></p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="feature-item">
                <i class="icon-present text-primary"></i>
                <h3>Vision</h3>
                <p class="text-muted"></p>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="feature-item">
                <i class="icon-heart text-primary"></i>
                <h3>Life</h3>
                <p class="text-muted"></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">
    var path = "{{ route('autocomplete') }}";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });
</script>
@endsection
