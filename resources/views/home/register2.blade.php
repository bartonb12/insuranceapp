@extends('new-design.new-partials.master')


@section('content')

<header class="masthead">
  <div class="container h-100">
    <div class="row h-100">
      <div class="col-lg-12 my-auto">
        <div class="header-content mx-auto">
          <form class="form-horizontal" method="POST" action="{{ route('register') }}">
              {{ csrf_field() }}
              <div class="col-md-12 col-md-offset-4">
              <h1 class="mb-5">Tell us about your insurance</h1>
            </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-12 control-label">Insurance Provider</label>

                  <div class="col-md-12">
                      <select id="insuranceProvider" class="form-control" name="insuranceProvider" required autofocus>
                      <option value="bcbskc">Blue Cross of Kansas City</option>
                    </select>
                  </div>
              </div>

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="col-md-12 control-label">Account Number</label>

                  <div class="col-md-12">
                      <input id="accountNumber" type="text" class="form-control" name="accountNumber" required>
                  </div>
              </div>

              <div class="form-group">
                  <div class="col-md-12 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                          Get Started!
                      </button>
                  </div>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</header>


@endsection
