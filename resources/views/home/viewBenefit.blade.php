
@extends('home.partials.master')


@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

<header class="masthead">
  <div class="container h-100">
    <div class="row h-100">
      <div class="col-lg-12 my-auto">
        <div class="header-content mx-auto">

<br><br>

          <br><br>
          <h2 class="mb-8">Results for {{ $benefitName }}</h2>
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Before Insurance</th>
                    <th scope="col">After Insurance</th>
                    <th scope="col">Service Provider</th>
                    <th scope="col">Location</th>
                    <th scope='col'>Distance</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($benefits as $benefit)
                  <tr>
                    <td>${{ $benefit->benefitCost }}</td>
                    <td>${{ $benefit->benefitCost * .3 }}</td>
                    <td>{{ $benefit->benefitProviderName }}</td>
                    <td>{{ $benefit->benefitProviderAddress1 }} <br> {{ $benefit->benefitProviderCity . ',' . $benefit->benefitProviderState}}</td>
                    <td>    <?php $response   = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' . urlencode($userLocation[0]->userAddress1) . $userLocation[0]->userCity . $userLocation[0]->userState . '&destinations=' . urlencode($benefit->benefitProviderAddress1) . $benefit->benefitProviderCity . $benefit->benefitProviderState . '&key=AIzaSyDwrRC2AGwK2ykscX8og_t1hiccCaSStBU');
                                  $response   = json_decode($response); ?>
                                {{ print_r($response->rows[0]->elements[0]->distance->text) }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
<br><br><hr>
              <form class="" action="/benefit"  method="post">
                {{ csrf_field() }}

              <h3 class="mb-5">Search for something else</h3>
              <input class="typeahead form-control" type="text" name='search'>
              <br>
              <input type="submit" class="btn btn-outline btn-xl js-scroll-trigger" value='Find Care Providers!'></input>

              </form>
        </div>
      </div>


    </div>
  </div>
</header>

<section class="features" id="features">
  <div class="container">
    <div class="section-heading text-center">
      <h2>Browse Benefits by Category</h2>
      <p class="text-muted">Check out what you can do with Insurapal!</p>
      <hr>
    </div>
    <div class="row">
      <div class="col-lg-12 my-auto">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-6">
              <div class="feature-item">
                <a href="#"><i class="icon-screen-smartphone text-primary"></i></a>
                <h3>Dental</h3>
                <p class="text-muted"></p>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="feature-item">
                <i class="icon-location-pin text-primary"></i>
                <h3>Health</h3>
                <p class="text-muted"></p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="feature-item">
                <i class="icon-present text-primary"></i>
                <h3>Vision</h3>
                <p class="text-muted"></p>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="feature-item">
                <i class="icon-heart text-primary"></i>
                <h3>Life</h3>
                <p class="text-muted"></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
    var path = "{{ route('autocomplete') }}";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });
</script>

@endsection
