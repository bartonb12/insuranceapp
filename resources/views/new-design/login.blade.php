@extends('new-design.new-partials.authMaster')


@section('content')
<header class="masthead">
  <div class="container h-100" style="vh:100;">
    <div class="row h-100">
      <div class="col-lg-7 my-auto">
        <div class="header-content mx-auto">
          <br>
          <br>
          <br><br>
          <form class="form-horizontal" method="POST" action="{{ route('login') }}">
              {{ csrf_field() }}

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                  <div class="col-md-8">
                      <input id="email" type="email" class="form-control login" name="email" value="{{ old('email') }}" required autofocus>

                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="col-md-4 control-label">Password</label>

                  <div class="col-md-8">
                      <input id="password" type="password" class="form-control login" name="password" required>

                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                          </label>
                      </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="col-md-8 col-md-offset-4">
                      <button type="submit" class="btn btn-outline btn-xl">
                          Login
                      </button>
<br>
<br>
                      <a class="btn btn-link" href="{{ route('password.request') }}">
                          Forgot Your Password?
                      </a>
                  </div>
              </div>
          </form>
        </div>
      </div>
      <div class="col-lg-5 my-auto">
        <div class="device-container">
          <h1 class="display-2" style="font-size: 4em;">Welcome to Wellness.</h1>
                <!-- You can hook the "home button" to some JavaScript events or just remove it -->
        </div>
      </div>
    </div>
  </div>
</header>

@endsection
