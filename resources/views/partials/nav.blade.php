<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
 <div class="container">
   <div class="navbar-header">
     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
       <span class="sr-only">Toggle navigation</span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
     </button>
     <a class="navbar-brand" href="/">Insurance App</a>
   </div>
   <div id="navbar" class="navbar-collapse collapse">
     <ul class="nav navbar-nav">
       <li <?php if ($page == 'home') {echo 'class="active"';} ?>><a href="/home">Home</a></li>
       <li <?php if ($page == 'about') {echo 'class="active"';} ?>><a href="/about">About</a></li>
       <li <?php if ($page == 'contact') {echo 'class="active"';} ?>><a href="/contact">Contact</a></li>
       @guest
       @else
       <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
         <ul class="dropdown-menu">
           <li><a href="/account">My Account</a></li>
           <li role="separator" class="divider"></li>
           <li class="dropdown-header">My Coverage</li>
           <li><a href="/dental"><span class="glyphicon glyphicon-apple" aria-hidden=true> Dental</span></a></li>
           <li><a href="/vision"><i class="glyphicon glyphicon-eye-open" aria-hidden=true> Vision</i></a></li>
           <li><a href="/general"><i class="fa fa-heartbeat" aria-hidden="true"> General</i></a></li>
           <li><a href="/medication"><i class="fa fa-medkit" aria-hidden="true"> Medication</i></a></li>
         </ul>
       </li>
       @endguest
     </ul>
     <ul class="nav navbar-nav navbar-right">
         <!-- Authentication Links -->
         @guest
             <li><a href="{{ route('login') }}">Login</a></li>
             <li><a href="{{ route('register') }}">Register</a></li>
         @else
             <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                     {{ Auth::user()->name }} <span class="caret"></span>
                 </a>

                 <ul class="dropdown-menu">
                     <li>
                         <a href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                             Logout
                         </a>

                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                             {{ csrf_field() }}
                         </form>
                     </li>
                 </ul>
             </li>
         @endguest
   </div><!--/.nav-collapse -->
 </div>
</nav>
