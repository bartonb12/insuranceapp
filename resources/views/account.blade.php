@extends('home.partials.master')


@section('content')

<header class="masthead">
  <div class="container h-100">

    <div class="row top">
      <div class="col-lg-7 my-auto">

        <div class="account">

          <h1 class="" style="font-size: 3.5em;">{{$user->name}}'s Account</h1>

          <div class="">
            See your personal info and your policy here.
          </div>
        </div>
      </div>
    </div>
<br><br>
    <div class="row">
      <div class="col-lg-7 my-auto">

        <div class="">
          <h3>Personal Info</h3>
          <p> <strong> Full Name: </strong> <br>
            {{$user->name}}</p>
          <p> <strong> Birthdate: </strong> <br>
            {{$user->policyNumber}}</p>
          <p> <strong> Address: </strong> </p>
          <address>
          {{$policy[0]->address1}}<br>
          {{$policy[0]->city}}, {{$policy[0]->state}}<br>
          {{$policy[0]->zipCode}}<br>
          {{$policy[0]->telephone}} <br>

          </address>

        </div>
      </div>

      <div class="col-lg-5 my-auto">
        <div class="">
          <h3>Insurance Info</h3>
          <p> <strong> Carrier: </strong> <br>
            {{$policy[0]->name}}</p>
          <p> <strong> Policy Number: </strong> <br>
            {{$user->policyNumber}}</p>
          <p> <strong> Address: </strong> </p>
          <address>
          {{$policy[0]->address1}}<br>
          {{$policy[0]->city}}, {{$policy[0]->state}}<br>
          {{$policy[0]->zipCode}}<br>
          {{$policy[0]->telephone}} <br>
          <a href="https://www.{{$policy[0]->url}}">{{$policy[0]->url}}</a>
          </address>

        </div>
      </div>

    </div>

  </div>
</header>


@endsection
