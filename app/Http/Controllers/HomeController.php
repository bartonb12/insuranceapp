<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\User;
use App\BenefitProvider;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function register2()
    {
        return view('home.register2');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function registered()
    {
        return view('home.home');
    }

     /**
     * Show a post
     *
     * @return View
     */
    public function account(User $user)
    {
      $providers  = BenefitProvider::all();
      $user       = Auth::user();
      $userPolicy = new User();
      $policy     = $userPolicy->policyInfo();
      // $response   = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' . $policy[0]->city . ',' . $policy[0]->state .'&destinations=New+York+City,NY&key=AIzaSyDwrRC2AGwK2ykscX8og_t1hiccCaSStBU');
      // $response   = json_decode($response);
      // $provdist   = [];
      // foreach ($providers as $provider) {
      //   $providercity = $provider->city;
      //   $providerstate = $provider->state;
      //   $distancefromhome = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' . $policy[0]->city . ',' . $policy[0]->state . '&destinations=' . $providercity . ',' . $providerstate . '&key=AIzaSyDwrRC2AGwK2ykscX8og_t1hiccCaSStBU');
      // }
      return view('account', compact('user', 'policy'));
    }
}
