<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Benefit;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;



class BenefitsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Create a new controller instance.
   *
   * @return view
   */
  public function benefit(Request $request)
  {
    $user        = Auth::user();
    $userLocation = DB::table('users')
              ->join('locations', 'users.locationid', '=', 'locations.id')
              ->select('locations.city as userCity',
                       'locations.state as userState',
                       'locations.address1 as userAddress1')
              ->where("users.id","=","{$user->id}")
              ->get();
    $benefitName = $request->input('search');
    $benefits    = DB::table('benefits')
              ->join('benefit_providers', 'benefits.benefitProviderId', '=', 'benefit_providers.id')
              ->join('locations', 'benefit_providers.location_id', '=', 'locations.id')
              ->select('benefits.name as benefitName',
                       'benefits.cost as benefitCost',
                       'benefit_providers.name as benefitProviderName',
                       'locations.city as benefitProviderCity',
                       'locations.state as benefitProviderState',
                       'locations.address1 as benefitProviderAddress1')
              ->where("benefits.name","LIKE","{$benefitName}")
              ->get();
    return view('home.viewBenefit', compact('benefitName', 'benefits', 'user', 'userLocation'));
  }
}
