<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =
    [
        'name',
        'email',
        'password',
        'insurancePolicyNumber',
        'birthdate',
        'isAdmin',
        'address1',
        'address2',
        'city',
        'state',
        'areaCode',
        'coverageId',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Testing a theory
     *
     * @return Some text
     */
    public function policyInfo()
    {
      return $policy = DB::table('users')
            ->join('insurance_policies', 'users.policyNumber', '=', 'insurance_policies.policyNumber')
            ->join('insurance_providers', 'insurance_policies.insuranceProviderId', '=', 'insurance_providers.id')
            ->join('locations', 'insurance_providers.location_id', '=', 'locations.id')
            ->select('*')
            ->get();
    }
}
