<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('new-design.index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/account', 'HomeController@account')->name('account');

Route::get('/about', 'StaticController@about')->name('about');
Route::get('/contact', 'StaticController@contact')->name('contact');

Route::get('search',array('as'=>'search','uses'=>'SearchController@search'));
Route::get('autocomplete',array('as'=>'autocomplete','uses'=>'SearchController@autocomplete'));

Route::post('/benefit', 'BenefitsController@benefit')->name('Benefit');

Route::get('api', function (){
    return ['Brock', 'Molly', 'Bentley', 'Baxter'];
});

Route::get('/vue/{any}', 'spaController@index')->where('any', '.*');

Route::get('/register2', 'HomeController@register2')->name('register2');
Route::post('/register2', 'HomeController@registered')->name('registered');
